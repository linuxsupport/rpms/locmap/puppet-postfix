Name:           puppet-postfix
Version:        1.5
Release:        1%{?dist}
Summary:        Masterless puppet module for postfix

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent
Requires:       puppet-augeas
Requires:       CERN-CA-certs

%description
Puppet postfix module for locmap

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/postfix/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/postfix/
touch %{buildroot}/%{_datadir}/puppet/modules/postfix/linuxsupport

%files -n puppet-postfix
%{_datadir}/puppet/modules/postfix
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Thu Feb 27 2025 CERN Linux Droid <linux.ci@cern.ch> - 1.5-1
- Rebased to #4d1d8e43 by locmap-updater

* Wed Feb 19 2025 CERN Linux Droid <linux.ci@cern.ch> - 1.4-1
- Rebased to #641b2f8a by locmap-updater

* Tue Oct 01 2024 Ben Morrice <ben.morrice@cern.ch> - 1.3-1
- add autoreconfigure %post script

* Fri Jan 26 2024 Ben Morrice <ben.morrice@cern.ch> - 1.2-1
- Rebased to #8491eaa6
- Fix for OTG0147112 (CRM-4745)

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.1-8
- Bump release for disttag change

* Thu Feb 03 2022 Ben Morrice <ben.morrice@cern.ch> - 1.1-7
- add CS9 support

* Wed Dec 01 2021 Ben Morrice <ben.morrice@cern.ch> - 1.1-6
- fix puppet duplicate resource in mta.pp

* Mon Nov 29 2021 Daniel Juarez <djuarezg@cern.ch> - 1.1-5
- Limit the number of outgoing connections
- Force encryption 

* Fri Sep 17 2021 Daniel Juarez <djuarezg@cern.ch> - 1.1-4
- Replicate module-base default mail config

* Fri Sep 03 2021 Daniel Juarez <djuarezg@cern.ch> - 1.1-3
- fix configuration to unblock unsent mail queue

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 1.1-2
- fix requires on puppet-agent

* Tue Feb 16 2021 Ben Morrice <ben.morrice@cern.ch> - 1.1-1
- move away from hiera lookups

* Mon Feb 10 2020 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
-Initial release
