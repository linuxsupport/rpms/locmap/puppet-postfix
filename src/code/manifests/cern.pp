class postfix::cern (
) {

  # Predefined configuration to match https://gitlab.cern.ch/ai/it-puppet-module-base/-/blob/master/code/manifests/mta.pp

  # Force use of tls to cernmx.cern.ch
  postfix::config{'smtp_tls_security_level':
    value => 'encrypt',
  }

  # Ignore the rpm generated self singed certificate
  #
  postfix::config{'smtpd_tls_cert_file':
    value => 'none',
  }
  postfix::config{'smtpd_tls_security_level':
    value => 'none',
  }

  # Limit to 5 outgoing connections
    postfix::config{'smtp_destination_concurrency_limit':
      value => '5',
  }

  postfix::config{'smtp_generic_maps':
    value => 'hash:/etc/postfix/generic',
  }
  postfix::hash{'/etc/postfix/generic':
    ensure  => present,
    content => ['root', 'adm'].map |$_id| { "${_id} ${_id}@${facts['networking']['fqdn']}" }.join("\n"),
  }

  postfix::config{'sender_canonical_maps':
    value => 'hash:/etc/postfix/sender_canonical',
  }
  postfix::hash{'/etc/postfix/sender_canonical':
    ensure => present,
  }
  postfix::canonical{'root':
    ensure      => present,
    destination => "root_${facts['networking']['hostname']}@cern.ch",
    file        => '/etc/postfix/sender_canonical',
  }

  # Deliver some mail locally to localhost.
  # They can then be configured with /etc/aliases and then running newalias
  postfix::virtual{'root':
    ensure      => present,
    destination => 'root@localhost',
  }

  postfix::virtual{'adm':
    ensure      => present,
    destination => 'adm@localhost',
  }

}
